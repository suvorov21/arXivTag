# arXiv tagger
A [web-page](https://suvorov21.gitlab.io/arXivTag/) for user-friendly monitoring of the paper submissions at [arXiv.org](https://arxiv.org/). Papers sorting, automate tag assignment, displaying articles since your last visit and other features.

## Features:
  1. Select a date range of submissions: today/this week/this month/**since your last visit**
  2. Add **any number of arXiv sections** and further toggle them easily with checkboxes
  3. Create **tags with rules for title, abstract, and author**. You can use logical operators (or/and), regular expressions, TeX formulas in rules. View the most interesting papers on top!
  4. **Sort the papers** with tags, submission dates, categories.
  5. All your preferences: categories, tags, checkboxes are saved and will be used on your next visit.

The website is hosted at [GitLab pages](https://suvorov21.gitlab.io/arXivTag/) and is fully functional. Your personal preferences are saved with cookies.

With minimal modifications in the HTML/CSS/JS you can get any paper representation format you want. The web-page with your own design can be used locally (below).

The code may be updated to any paper submission system that has API.

## Development

In order to participate the development I encourage you to use a local Flask server for testing. Make sure that Flask is installed (`python3 -m pip install flask flask-Cors`). To start the server run:

```bash
git clone https://gitlab.com/suvorov21/arXivTag.git
cd arXivTag/arXivTag
python3 server_pure_js.py
```

and open provided link (e.g. `http://127.0.0.1:8880/`) with your browser.
