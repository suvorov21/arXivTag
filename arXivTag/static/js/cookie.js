/*global MathJax*/
/*eslint no-undef: "error"*/

var prefs = {

  data: {},

  load(name="prefs") {
    let cookieDecoded = decodeURIComponent(document.cookie).split(";");
    let cname = name + "=";
    for (let i = 0; i < cookieDecoded.length; i++) {
      let cook = cookieDecoded[parseInt(i, 10)];
      while (cook[0] === " ") {
        cook = cook.slice(1);
      }
      if (cook.indexOf(cname) === 0) {
        this.data = JSON.parse(cook.substring(cname.length, cook.length));
      }
    }

    return this.data;
  },

  save(expires, path) {
    var d = expires || new Date(2100, 2, 2);
    var p = path || "/";
    document.cookie = "prefs=" + encodeURIComponent(JSON.stringify(this.data))
              + ";expires=" + d.toUTCString()
              + ";path=" + p;
  }
};

prefs.load();
// list of settings
// 1. Category list of {"name", "show"}
// 2. Tag list of {"name", "rule", "color"}
// 3. ParseTex bool
// 4. parseTexPar bool
// 5. Render abstracts on load
// 6. Last visit date
// 7. Last paper seen
// 8. simpleRule - simplified & rule in tag


// assign default params in absense of cookies
if (!prefs.data.hasOwnProperty("cats")) {
  prefs.data.cats = [{"name": "hep-ex", "show": true},
                     {"name": "hep-ph", "show": true},
                     {"name": "physics.ins-det", "show": true}];
}

if (!prefs.data.hasOwnProperty("tags")) {
  prefs.data.tags = [{"name": "acc. $\\nu$",
                      "rule": "abs{accelerator&neutrino}",
                      "color": "#ffaaaa"},
                      {"name": "$\\nu$-phys",
                     "rule": "ti{neutrino|\\nu}|abs{neutrino|\\nu}",
                     "color": "#aaffaa"
                     },
                     {"name": "LHC-ex",
                      "rule": "au{ATLAS|CMS|LHCB|ALICE}|ti{ATLAS|CMS|LHCB|ALICE}",
                      "color": "#aaaaff"},
                     {"name" : "CERN",
                      "rule": "ti{CERN}|abs{CERN}",
                      "color": "#ffffaa"}
                      ];
}

if (!prefs.data.hasOwnProperty("parseTex")) {
  prefs.data.parseTex = true;
}

if (!prefs.data.hasOwnProperty("parseTexPar")) {
  prefs.data.parseTexPar = false;
}

if (!prefs.data.hasOwnProperty("renderAbs")) {
  prefs.data.renderAbs = true;
}

if (!prefs.data.hasOwnProperty("lastVisit")) {
  prefs.data.lastVisit = new Date(2000, 1, 1);
}

if (!prefs.data.hasOwnProperty("lastPaper")) {
  prefs.data.lastPaper = new Date(2000, 1, 1);
}

if (!prefs.data.hasOwnProperty("showNov")) {
  prefs.data.showNov = [true, true, true];
}

if (!prefs.data.hasOwnProperty("simpleRule")) {
  prefs.data.simpleRule = true;
}

prefs.save();

function parseTex() {
  // console.time("tex");

  setTimeout( function() {
    if (prefs.data.parseTex) {
      // WARNING
      // usually typeset() is working. It processes laTeX in multiple threads --> fast
      // If there are dependencies (e.g. \newcommand{}) consequent typesetPromise() should be used.
      if (prefs.data.parseTexPar) {
        MathJax.typeset();
      } else {
        MathJax.typesetPromise();
      }
    }
  }, 0);
    // console.timeEnd("tex");
}

// open settings
$("#set-block").click((event) => {
  document.location.href = "settings.html";
});
// open help
$("#info-block").click((event) => {
  document.location.href = "help.html";
});
