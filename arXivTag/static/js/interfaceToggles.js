/*global prefs, MathJax, displayPapers, sortPapers, loadPapers, toggleVis, DATA, parseTex, reloadData*/
/*eslint no-undef: "error"*/

// change sort selector
$("#sort-sel").change((event) => {
  $("#sorting-proc").css("display", "block");
  sortPapers();
  // WARNING
  // dirty fix to throw displayPapers() in the separate thread
  // and not frise the selector
  setTimeout(function () {
    displayPapers();
    $("#sorting-proc").css("display", "none");
    parseTex();
  }, 0);
});

// toggle abstract visibility
$("#paper-list").click((event) => {
  if (!$(event.target).hasClass("btn-abs")) {
    return true;
  }
  var num = $(event.target).attr("id").split("-")[2];
  var name =  "#abs-" + num;

  if (typeof $(name).html() === "undefined") {
    // create the div, parse TeX, then show
    var abs = document.createElement("div");
    abs.setAttribute("class", "paper-abs");
    abs.setAttribute("id", name.slice(1));
    abs.textContent = DATA.papers[parseInt(num, 10)].abstract;
    $("#paper-"+num).append(abs);
    if (prefs.data.parseTex) {
      MathJax.typeset();
    }
  }

  if ($(name).css("display") === "none") {
    $(name).css("display", "block");
  } else {
    $(name).css("display", "none");
  }
});

$("#date-today").click((event) => {
  reloadData();
  loadPapers(1);
});

$("#date-week").click((event) => {
  reloadData();
  loadPapers(2);
});

$("#date-month").click((event) => {
  reloadData();
  loadPapers(3);
});

$("#date-last").click((event) => {
  reloadData();
  loadPapers(4);
});


// make menu labels clickable
$(".item-nov").click((event) => {
  var name =  "#check-nov-" + $(event.target).attr("id").split("-")[1];
  $(name).click();
});

// Toggle visibility with novelty checks click
$(".check-nov").click((event) => {
  var number = $(event.target).attr("id").split("-")[2];
  prefs.data.showNov[parseInt(number, 10)] = $("#check-nov-"+number).is(":checked");
  prefs.save();
  setTimeout(function () {
    toggleVis();
  }, 0);
});

