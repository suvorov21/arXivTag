/*global prefs, MathJax, parseTex*/
/*eslint no-undef: "error"*/

function alertLocalError(jqXHR, URL) {
  $("#loadPapers").css("display", "None");
  var error = "</br></br>arXiv server error</br>";
  error += "Status: " + jqXHR.status + "</br></br>";
  error += "Requested API: " + URL;
  $("#paper-list-content").html(error);
}

function formateDate(date) {
  let dateArray = date.toString().split(" ");
  return dateArray[2] + " " + dateArray[1] + " " + dateArray[3];
}

function escapeHtml(text) {
  var map = {
    "&amp;": "&",
    "&lt;": "<",
    "&gt;": ">",
    "&quot;": '"',
    "&#039;": "'"
  };
  return text.replace(/(&amp|&lt;|&gt;|&quot;|&#039;)/g, function(m) { return map[m]; });
}

function fixXML(text) {
  let regex = RegExp("\n", "g");
  return escapeHtml(text.replace(regex, " "));
}


var DATA = {};
DATA.papers = [];
var catsArr = [];
var oldDate = new Date(1970, 1, 1);
var START=0;
var maxResults=100;

// refresh data for new reload
function reloadData() {
  DATA={};
  DATA.papers=[];
  START = 0;
  maxResults = 100;
  oldDate = new Date(1970, 1, 1);
  $("#sort-block").css("display", "none");
  $("#no-paper").css("display", "none");
  $("#load_papers").css("display", "block");
  $("#paper-list-content").html("");
}

prefs.data.showNov.forEach((content, num) => {
  if (!content) {
    $("#check-nov-"+num).prop("checked", false);
  }
});

// last visit date
$("#last-visit-date").html(formateDate(new Date(prefs.data.lastVisit)));

function toggleVis() {
  // console.time("toggleVis");
  $("#no-paper").css("display", "none");

  var passed = 0;
  var paperId = 0;
  for (paperId = 0; paperId < DATA.papers.length; paperId++) {
    var paper = DATA.papers[parseInt(paperId, 10)];
    var show = false;
    // 1st check by category
    // for(var cat in showCats) {
    for (let id = 0; id < prefs.data.cats.length; id++) {
      let cat = prefs.data.cats[parseInt(id, 10)];
      if (!cat.show) {
        continue;
      }

      if (!prefs.data.showNov[1] && cat.name !== paper.cats[0]) {
        continue;
      }

      var catId = 0;
      for (catId = 0; catId < paper.cats.length; catId++) {
        if (paper.cats[parseInt(catId, 10)] === cat.name && cat.show) {
          show = true;
          break;
        }

      } // loop over all cats
      if (show) {
        break;
      }
    } // loop over paper cats

    if (!show) {
      $("#paper-"+paperId).css("display", "None");
      continue;
    }

    // 2nd check by novelty
    if (prefs.data.showNov[1] && paper.cross ||
        prefs.data.showNov[2] && paper.up ||
        prefs.data.showNov[0] && !paper.cross & !paper.up) {
      // BA DUM TSSS
      // don't want to invert logic, too lazy
    } else {
      show = false;
    }

    if (!show) {
      $("#paper-"+paperId).css("display", "None");
      continue;
    }

    // 3rd check by tag
    // TODO

    passed += 1;
    var title = $("#paper-title-"+paperId).html();
    var n = title.indexOf(".");
    $("#paper-title-"+paperId).html(passed + title.slice(n));
    $("#paper-"+paperId).css("display", "block");

  } // paper loop

  if (passed === 0) {
    $("#sort-block").css("display", "none");
    $("#no-paper").css("display", "block");
    $("#load_papers").css("display", "none");
  } else {
    $("#load_papers").css("display", "none");
    $("#no-paper").css("display", "none");
    $("#sort-block").css("display", "block");
    $("#passed").html(passed);
  }
  // console.timeEnd("toggleVis");
}

// show categories according to preferences
function renderCats() {
  prefs.data.cats.forEach((cat, num) => {
    catsArr.push(cat.name);

    var parent = document.createElement("div");
    parent.setAttribute("class", "menu-item");

    var check = document.createElement("input");
    check.setAttribute("type", "checkbox");
    check.setAttribute("id", "check-cat-"+num);
    check.setAttribute("class", "check-cat");
    if (cat.show) {
      check.checked = true;
    }
    check.onchange = function() {
      prefs.data.cats[parseInt(num, 10)].show = $("#check-cat-"+num).is(":checked");
      prefs.save();
      setTimeout(function () {
        toggleVis();
      }, 0);
    };

    var catElement = document.createElement("div");
    catElement.setAttribute("class", "menu-line-left item-cat");
    catElement.setAttribute("id", "cat-label-"+num);
    catElement.textContent = cat.name;
    catElement.onclick = function() {
      var number = $(this).attr("id").split("-")[2];
      var name =  "#check-cat-" + number;
      $(name).click();
    };

    var counter = document.createElement("div");
    counter.setAttribute("class", "menu-line-right");
    counter.setAttribute("id", "cat-count-"+num);
    counter.textContent = "0";

    $("#cats").append(parent);
    parent.append(check);
    parent.append(catElement);
    parent.append(counter);
  });
}

// show tags from preferences
function renderTags() {
  prefs.data.tags.forEach((tag, num) => {
    var parent = document.createElement("div");
    parent.setAttribute("class", "menu-item");

    var tagElement = document.createElement("div");
    tagElement.setAttribute("class", "menu-line-left tag-label");
    tagElement.setAttribute("id", "tag-label-"+num);
    tagElement.setAttribute("style", "background-color: " + tag.color + "; margin: 0;");
    tagElement.textContent = tag.name;

    var counter = document.createElement("div");
    counter.setAttribute("class", "menu-line-right");
    counter.setAttribute("id", "tag-count-"+num);
    counter.textContent = "0";

    $("#tags").append(parent);
    parent.append(tagElement);
    parent.append(counter);
  });
}

function updateVisit(todayPaperDate) {
  prefs.data.lastPaper = todayPaperDate;
  prefs.data.lastVisit = new Date();
  prefs.save();
  $("#last-visit-date").html(formateDate(new Date(prefs.data.lastVisit)));
}

// arXiv API usage
// separated from loadPapers()
// keep all the arXiv-related params here
function getPapersArxiv(dateType=-1) {
  if (dateType === 2) {
    maxResults = 400;
  } else if (dateType === 3) {
    maxResults = 900;
  }

  var URL = "https://export.arxiv.org/api/query?search_query=cat:" +
            prefs.data.cats.map((cat) => {return cat.name;}).join("%20OR%20") +
            "&sortBy=lastUpdatedDate&sortOrder=descending" + "&start="+START +
            "&max_results=" + maxResults;

  // console.log(URL);

  let continueLoad = true;

  $.ajax({
    url: URL,
    type: "get",
    success(data) {
      // console.time("parsePapers");
      //parse HTML collection into dictionary
      let papers = data.getElementsByTagName("entry");
      if (dateType > 0) {
        // define the last paper date
        let paper = papers[0];
        let todayPaperDate = new Date(paper.getElementsByTagName("updated")[0].innerHTML);
        oldDate = new Date();
        // if last visit far in the past, update it
        if (new Date(prefs.data.lastPaper) < new Date(2010, 1, 1)) {
          updateVisit(todayPaperDate);
        }

        if (dateType === 1) {
          // today papers
          oldDate = new Date(todayPaperDate.setDate(todayPaperDate.getDate() - 1));
        } else if (dateType === 2) {
          // this week
          // if last paper is published on Friday
          // "this week" starts from next Monday (papers published from Thursday)
          if (todayPaperDate.getDay() === 5) {
            oldDate = new Date(todayPaperDate.setDate(todayPaperDate.getDate() - 1));
          } else {
            oldDate = new Date(todayPaperDate.setDate(todayPaperDate.getDate() - todayPaperDate.getDay() - 2));
          }
        } else if (dateType === 3) {
          // this month
          oldDate = new Date(todayPaperDate.setDate(todayPaperDate.getDate() - todayPaperDate.getDate()));
        } else if (dateType === 4) {
          oldDate = new Date(prefs.data.lastPaper);
          updateVisit(todayPaperDate);
        }

        // over weekend cross
        if (oldDate.getDay() > 4) {
          oldDate = new Date(oldDate.setDate(oldDate.getDate() - oldDate.getDay() + 4));
        }
        // "last day" === Sunday, then look for papers since Friday
        if (oldDate.getDay() === 0) {
          oldDate = new Date(oldDate.setDate(oldDate.getDate() - 2));
        }

        // papers are submitted by 18:00Z
        oldDate.setHours(17);
        oldDate.setMinutes(59 - oldDate.getTimezoneOffset());
        oldDate.setSeconds(59);
      }


      for (let paperId = 0; paperId < papers.length; paperId++) {
        let paper = papers[parseInt(paperId, 10)];

        // check if relevant
        if (new Date(paper.getElementsByTagName("updated")[0].innerHTML) < oldDate) {
          $(document).trigger("papersLoaded");
          continueLoad = false;
          break;
        }

        // parse authors
        let author = Array.from(paper.getElementsByTagName("author"));
        author = author.map((au) => {return au.getElementsByTagName("name")[0].innerHTML;});
        author = author.join(", ");

        // parse links
        var pdfLink = "";
        var absLink = "";
        let links = paper.getElementsByTagName("link");
        for (let linkId = 0; linkId < links.length; linkId++) {
          if (links[parseInt(linkId, 10)].getAttribute("rel") === "related" &&
              links[parseInt(linkId, 10)].getAttribute("title") === "pdf") {
            pdfLink = links[parseInt(linkId, 10)].getAttribute("href");
          }
          if (links[parseInt(linkId, 10)].getAttribute("rel") === "alternate") {
            absLink = links[parseInt(linkId, 10)].getAttribute("href");
          }
        }

        // parse categories
        let paperCatsArr = [];
        let cats = paper.getElementsByTagName("category");
        for (let catId = 0; catId < cats.length; ++catId) {
          paperCatsArr.push(cats[parseInt(catId, 10)].getAttribute("term"));
        }

        // fill dictionary
        DATA.papers.push({
          "title": fixXML(paper.getElementsByTagName("title")[0].innerHTML),
          "dateSub": new Date(paper.getElementsByTagName("published")[0].innerHTML),
          "dateUp": new Date(paper.getElementsByTagName("updated")[0].innerHTML),
          "abstract": fixXML(paper.getElementsByTagName("summary")[0].innerHTML),
          author,
          pdfLink,
          absLink,
          "cats": paperCatsArr,
          // additional info to be filled later
          "tags": [],
          "cross": false,
          "up": false
        });
      }
      // console.timeEnd("parsePapers");
    },
    error(jqXHR) {
      alertLocalError(jqXHR, URL);
    },
    complete(data) {
      if (continueLoad) {
        START += maxResults;
        setTimeout(getPapersArxiv, 2000);
      }
    }
  });

  // display proper title
  if (dateType === 1) {
    $("#paper-list-title").html("Papers for today: " + formateDate(new Date()));
  } else if (dateType === 2) {
    let date = new Date();
    let dateNew = new Date();
    let start = new Date(date.setDate(date.getDate() - date.getDay() + 1));
    let end = new Date(dateNew.setDate(dateNew.getDate() + 7 - dateNew.getDay()));
    // Sunday (Burn in hell JS!!!)
    if ((new Date()).getDay() === 0) {
      start = new Date(start.setDate(start.getDate() - 7));
      end = new Date(dateNew.setDate(end.getDate() - 7));
    }
    $("#paper-list-title").html("Papers for this week: " + formateDate(start) + " - " + formateDate(end));
  } else if (dateType === 3) {
    let date = new Date();
    let end = new Date;
    let start = new Date(date.setDate(date.getDate() - date.getDate() + 1));

    $("#paper-list-title").html("Papers for this month: " + formateDate(start) + " - " + formateDate(end));
  } else if (dateType === 4) {
    $("#paper-list-title").html("Papers since last visit on " + formateDate(new Date(prefs.data.lastVisit)));
  }

}

// show number of categories and tags
function displayCounters() {
  for (let i = 0; i < catsArr.length; i++) {
    $("#cat-count-"+i).html(DATA.nCat[parseInt(i, 10)]);
  }

  $("#nov-count-0").html(DATA.nNew);
  $("#nov-count-1").html(DATA.nCross);
  $("#nov-count-2").html(DATA.nUp);

  for (let i = 0; i < prefs.data.tags.length; i++) {
    $("#tag-count-"+i).html(DATA.nTag[parseInt(i, 10)]);
  }
}

function sortFunction(a, b, order=true) {
  return order? a - b : b - a;
}

function sortPapers() {
  // console.time("sort");
  let sortMethod = $("#sort-sel").val();
  // tag increase
  if (sortMethod.includes("tag")) {

    DATA.papers.sort((a, b) => {
      if (b.tags.length === 0) {
        return -1;
      }
      if (a.tags.length === 0) {
        return 1;
      }
      return sortFunction(a.tags[0], b.tags[0],
                          sortMethod === "tag-as"? true : false);
    });
  }
  // dates
  if (sortMethod.includes("date")) {
    DATA.papers.sort((a, b) => {
      var aDate = new Date(a.dateUp);
      var bDate = new Date(b.dateUp);
      return sortFunction(aDate, bDate,
                          sortMethod === "date-des"? true : false);
    });
  }

  // caregories
  if (sortMethod.includes("cat")) {
    DATA.papers.sort((a, b) => {
      let catA = "";
      let catB = "";
      for (let id = 0; id < a.cats.length; id++) {
        if (catsArr.includes(a.cats[parseInt(id, 10)])) {
          catA = a.cats[parseInt(id, 10)];
          break;
        }
      }
      for (let id = 0; id < b.cats.length; id++) {
        if (catsArr.includes(b.cats[parseInt(id, 10)])) {
          catB = b.cats[parseInt(id, 10)];
          break;
        }
      }
      return sortFunction(catsArr.indexOf(catA), catsArr.indexOf(catB),
                          sortMethod === "cat-as"? true : false);
    });
  }

  // Novelty new-crossКуа-update
  if (sortMethod.includes("nov")) {
    DATA.papers.sort((a, b) => {
      let novA = a.up ? 3 : a.cross? 2 : 1;
      let novB = b.up ? 3 : b.cross? 2 : 1;
      return sortFunction(novA, novB,
                          sortMethod === "nov-des"? true : false);
    });
  }
  // console.timeEnd("sort");
}

function displayPapers() {
  // console.time("display");
  $("#paper-list-content").html("");
  var content = "";
  DATA.papers.forEach(function(content, i) {
    var paper = document.createElement("div");
    paper.setAttribute("class", "paper");
    paper.setAttribute("id", "paper-"+i);

    var title = document.createElement("div");
    title.setAttribute("class", "paper-title");
    title.setAttribute("id", "paper-title-"+i);
    var str = (i+1) + ". " + content.title;
    title.textContent = str;

    var tagPanel = document.createElement("div");
    tagPanel.setAttribute("class", "tag-panel");
    tagPanel.setAttribute("id", "tag-panel-"+i);
    var tags = [];
    content.tags.forEach(function(tag, tagId) {
      var tagDiv = document.createElement("div");
      tagDiv.setAttribute("class", "tag-panel-item");
      tagDiv.setAttribute("style", "background-color:" + prefs.data.tags[parseInt(tag, 10)].color);
      tagDiv.textContent = prefs.data.tags[parseInt(tag, 10)].name;
      tags.push(tagDiv);
    });

    var au = document.createElement("div");
    au.setAttribute("class", "paper-au");
    if (content.author.split(", ").length > 4) {
      let author = content.author.split(", ").slice(0, 4).join(", ") + ", et al";
      au.textContent = author;
    } else {
      au.textContent = content.author;
    }

    var grid = document.createElement("div");
    grid.setAttribute("class", "paper-grid");

    var cat = document.createElement("div");
    cat.setAttribute("class", "paper-cat");
    cat.innerHTML = "[<strong>" + content.cats[0] + "</strong>";
    if (content.cats.length > 1) {
      cat.innerHTML += ", ";
    }
    cat.innerHTML += content.cats.slice(1).join(", ");
    cat.innerHTML += "]";

    var date = document.createElement("div");
    date.setAttribute("class", "paper-date");
    date.textContent = formateDate(content.dateUp);

    if (formateDate(content.dateSub) !== formateDate(content.dateUp)) {
      date.textContent += " (v1: " + formateDate(content.dateSub) + ")";
    }

    var btnPanel = document.createElement("div");
    btnPanel.setAttribute("class", "btn-panel");
    btnPanel.setAttribute("id", "btn-panel-"+i);

    var btnAbs = document.createElement("button");
    btnAbs.setAttribute("class", "btn btn-primary btn-abs");
    btnAbs.setAttribute("id", "btn-abs-"+i);
    btnAbs.textContent = "Abstract";

    var btnPdf = document.createElement("a");
    btnPdf.setAttribute("class", "btn btn-primary");
    btnPdf.setAttribute("id", "btn-pdf-"+i);
    btnPdf.setAttribute("href", content.pdfLink);
    btnPdf.setAttribute("target", "_blank");
    btnPdf.textContent = "PDF";

    var btnArxiv = document.createElement("a");
    btnArxiv.setAttribute("class", "btn btn-primary");
    btnArxiv.setAttribute("id", "btn-arxiv-"+i);
    btnArxiv.setAttribute("href", content.absLink);
    btnArxiv.setAttribute("target", "_blank");
    btnArxiv.textContent = "arXiv";

    var abs;
    if (prefs.data.renderAbs) {
      abs = document.createElement("div");
      abs.setAttribute("class", "paper-abs");
      abs.setAttribute("id", "abs-"+i);
      abs.textContent = content.abstract;
    }

    document.getElementById("paper-list-content").appendChild(paper);
    document.getElementById("paper-"+i).appendChild(title);
    document.getElementById("paper-"+i).appendChild(tagPanel);
    tags.forEach(function(tag){
      document.getElementById("tag-panel-"+i).appendChild(tag);
    });
    if (tags.length === 0) {
      tagPanel.setAttribute("style", "display: none");
    }
    document.getElementById("paper-"+i).appendChild(au);
    document.getElementById("paper-"+i).appendChild(date);
    document.getElementById("paper-"+i).appendChild(cat);

    document.getElementById("paper-"+i).appendChild(btnPanel);
    document.getElementById("btn-panel-"+i).appendChild(btnAbs);
    document.getElementById("btn-panel-"+i).appendChild(btnPdf);
    document.getElementById("btn-panel-"+i).appendChild(btnArxiv);
    if (prefs.data.renderAbs) {
      document.getElementById("paper-"+i).appendChild(abs);
    }
  });
  // console.timeEnd("display");

  parseTex();
}

function separateRules(rule, sign) {
  let signPos = rule.search("\\}\\" + sign + "(ti|abs|au)")+1;

  let reverseRule = rule.slice(0, signPos).split("").reverse().join("");
  let fst = reverseRule.match(".*?\\{(.*?)(&|\\||$)")[0];
  fst = fst.split("").reverse().join("");

  let scd = rule.slice(signPos+1).split("}")[0] + "}";

  let fstStart = signPos - fst.length;
  let scdEnd = signPos + scd.length;

  return [fst, scd, rule.slice(0, fstStart), rule.slice(scdEnd+2)];
}

function parseSimpleRule(paper, rule, prefix) {
  let ruleDict = {"ti": "title",
                  "au": "author",
                  "abs": "abstract"
                  };
  let searchTarget = paper[ruleDict[prefix]];
  rule = rule.replace(prefix+"{", "");
  rule = rule.replace("}", "");

  if (prefs.data.simpleRule && rule.includes("&")) {
    let tmpArr = rule.split("&");
    rule = "(" + rule.replace("&", ".*") + ")|(" + tmpArr.reverse().join(".*") + ")";
  }

  let regex = new RegExp(rule, "i");
  if (searchTarget.search(regex) !== -1) {
    return true;
  }
  return false;
}


function tagSuitable(paper, rule) {
  if (rule === "true") {
    return true;
  }
  if (rule === "false") {
    return false;
  }

  let ruleOutsideCurlyArr = [...rule.matchAll("(.*?)\{.*?\}")];
  let ruleOutsideCurly = "";
  ruleOutsideCurlyArr.forEach((match) => {
    ruleOutsideCurly += match[1];
  });

  // parse brackets
  if (ruleOutsideCurly.includes("(")) {
    let startPos = rule.search("(.*?)\\{.*?\\}");
    let endPos = rule.search("\\}(\\))");
    let condition = (tagSuitable(paper, rule.slice(startPos+1, endPos))).toString();
    var newRule = rule.slice(0, startPos) + condition + rule.slice(endPos=1);
    return tagSuitable(paper, newRule);
  }

  // parse logic AND
  if (ruleOutsideCurly.includes("&")) {
    let arr = separateRules(rule, "&");
    let fst = arr[0];
    let scd = arr[1];
    let beforeFst = arr[2];
    let afterScd = arr[3];
    let condition = (tagSuitable(paper, fst) && tagSuitable(paper, scd)).toString();
    let newRule = beforeFst + condition + afterScd;
    return tagSuitable(paper, newRule);
  }

  // parse logic OR
  if (ruleOutsideCurly.includes("|")) {
    let arr = separateRules(rule, "|");
    let fst = arr[0];
    let scd = arr[1];
    let beforeFst = arr[2];
    let afterScd = arr[3];
    let condition = (tagSuitable(paper, fst) || tagSuitable(paper, scd)).toString();
    let newRule = beforeFst + condition + afterScd;
    return tagSuitable(paper, newRule);
  }

  // parse simple conditions ti/abs/au
  let match = rule.match("^(ti|abs|au)\\{.*?\\}");
  if (match !== null) {
    return parseSimpleRule(paper, rule, match[1]);
  }

  return false;
}

function processPapers(date) {
  // console.time("process");
  // initialize
  DATA.nCat = [];
  catsArr.forEach((cat) => {DATA.nCat.push(0);});
  DATA.nTag = [];
  prefs.data.tags.forEach((tag) => {DATA.nTag.push(0);});
  DATA.nCross = 0;
  DATA.nNew = 0;
  DATA.nUp = 0;

  // process
  DATA.papers.forEach((paper) => {
    let cross = false;
    let up = false;
    paper.cats.forEach((paperCat) => {
      // cats counter
      let id = catsArr.indexOf(paperCat);
      if (id !== -1) {
        DATA.nCat[parseInt(id)] += 1;
      }
    });

    // count cross-refs
    if (catsArr.indexOf(paper.cats[0]) === -1) {
      cross = true;
      paper.cross = true;
      DATA.nCross += 1;
    }

    if (paper.dateSub < oldDate) {
      up = true;
      paper.up = true;
      DATA.nUp += 1;
    }

    if (!up && !cross) {
      DATA.nNew += 1;
    }

    // tag assignment
    prefs.data.tags.forEach((tag, num) => {
      if (tagSuitable(paper, tag.rule)){
        paper.tags.push(num);
        DATA.nTag[parseInt(num)] += 1;
      }
    });
  });
  setTimeout(function() {
    displayCounters();
    sortPapers();
    displayPapers();
    toggleVis();
  }, 0);
  // console.timeEnd("process");
}

function loadPapers(date=1) {
  getPapersArxiv(date);
}

$(document).ready((event) => {
  reloadData();
  renderCats();
  renderTags();
  loadPapers(1);
});

$(document).on("papersLoaded", (event) => {
  setTimeout(function() {
    processPapers();
  }, 0);
});
