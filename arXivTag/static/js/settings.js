/*global prefs, MathJax, allCatsArray, parseTex*/
/*eslint no-undef: "error"*/

var dataNew;
var deleteCatId;

$(document).on("click", ".del", (event) => {
  deleteCatId = $(event.target).attr("id").split("-")[2];
  let catName = dataNew.cats[parseInt(deleteCatId, 10)].name;
  $("#del-name").html("category <strong>" + catName + "</strong>");
  $("#settings-pop").css("display", "block");
});

function renderCatsSet() {
  // console.time("render");
  $("#cats-list").html("");
  dataNew.cats.forEach((cat, num) => {
    $("#cats-list").append("<div class='cat-item' id='cat-set-" + num + "'></div>");
    $("#cat-set-" + num).append("<div class='item-left del' style='margin-right:10px' id='cat-del-" + num + "'>&#10008;</div>");
    $("#cat-set-" + num).append("<div class='item-left'>" + cat.name + "</div>");
  });

  $.each(allCatsArray, function(val, text) {
    $("#catsDataList").append($("<option>").attr("value", val).text(text));
  });

  $("#cats-list").append("<div class='cat-item' id='cat-add'></div>");
  $("#cat-add").append("<input list='catsDataList' id='cat-add-input' />");
  $("#cat-add").append("<button class='btn btn-primary' id='submit-cat'>Add</button>");
  $("#submit-cat").on("click", function() {
    let catName = $("#cat-add-input").val();
    if (!allCatsArray.hasOwnProperty(catName)) {
      $("#info-name").html("Category not found. Please use one from the suggested list.");
      $("#info-pop").css("display", "block");
    } else if (dataNew.cats.map((cat) => {return cat.name;}).includes(catName)) {
      $("#info-name").html("Category already added!");
      $("#info-pop").css("display", "block");
    } else {
      dataNew.cats.push({"name": catName, "show": true});
      renderCatsSet();
    }
  });
  // console.timeEnd("render");
}

$("#btn-confirm").on("click", (event) => {
  dataNew.cats.splice(deleteCatId, 1);
  $("#settings-pop").css("display", "none");
  renderCatsSet();
});

var newTag = true;
var editTagId = -1;

$("#tags-list").click((event) => {
  if (!$(event.target).attr("class").includes("tag-label")) {
    return;
  }
  let tagCol = document.getElementsByClassName("tag-label");
  for (let id = 0; id < tagCol.length; id++) {
    $("#tag-label-"+parseInt(id, 10)).css("border-color", "transparent");
    if ($(tagCol[parseInt(id, 10)]).attr("id") === "add-tag") {
      $("#add-tag").css("border-style", "dashed");
      $("#add-tag").css("border-width", "2px");
    }
  }

  if ($(event.target).attr("id") === "add-tag") {
    newTag = true;
    $("#btn-reset").click();
    $("#add-tag").css("border-style", "solid");
    $("#add-tag").css("border-width", "4px");
  } else {
    newTag = false;
    editTagId = $(event.target).attr("id").split("-")[2];
    let tag = dataNew.tags[parseInt(editTagId, 10)];

    $(event.target).css("border-color", "#000");

    $("#tag-fields").prop("disabled", false);
    document.forms["add-tag"]["tag_name"].value = tag.name;
    document.forms["add-tag"]["tag_rule"].value = tag.rule;
    document.forms["add-tag"]["tag_color"].value = tag.color;
    document.forms["add-tag"]["tag_order"].value = editTagId;
    $("#tag-color").css("background-color", $("#tag-color").val());
  }
  $("#tag-fields").prop("disabled", false);
});

// show tags from preferences
function renderTagsSet() {
  $("#tags-list").html("");
  dataNew.tags.forEach((tag, num) => {
    var parent = document.createElement("div");

    var tagElement = document.createElement("div");
    tagElement.setAttribute("class", "tag-label");
    tagElement.setAttribute("id", "tag-label-"+num);
    tagElement.setAttribute("style", "background-color: " + tag.color + ";");
    tagElement.textContent = tag.name;

    $("#tags-list").append(parent);
    parent.append(tagElement);
  });
  var parent = document.createElement("div");

  var tagElement = document.createElement("div");
  tagElement.setAttribute("class", "add-set tag-label");
  tagElement.setAttribute("id", "add-tag");
  tagElement.textContent = "New";

  $("#tags-list").append(parent);
  parent.append(tagElement);

  parseTex();
}

function renderPerf() {
  if (prefs.data.parseTex) {
    $("input[name='tex'][value=on]").prop("checked", true);
  } else {
    $("input[name='tex'][value=off]").prop("checked", true);
  }

  if (prefs.data.parseTexPar) {
    $("input[name='texPar'][value=on]").prop("checked", true);
  } else {
    $("input[name='texPar'][value=off]").prop("checked", true);
  }

  if (prefs.data.renderAbs) {
    $("input[name='renderAbs'][value=on]").prop("checked", true);
  } else {
    $("input[name='renderAbs'][value=off]").prop("checked", true);
  }

  if (prefs.data.simpleRule) {
    $("input[name='simpleRule'][value=on]").prop("checked", true);
  } else {
    $("input[name='simpleRule'][value=off]").prop("checked", true);
  }
}

$(".latex-rad").click((event) => {
  dataNew.parseTex = $("input[type='radio'][name='tex']:checked").val() === "on" ? true : false;
});

$(".texPar-rad").click((event) => {
  dataNew.parseTexPar = $("input[type='radio'][name='texPar']:checked").val() === "on" ? true : false;
});

$(".renderAbs-rad").click((event) => {
  dataNew.renderAbs = $("input[type='radio'][name='renderAbs']:checked").val() === "on" ? true : false;
});

$(".simpleRule-rad").click((event) => {
  dataNew.simpleRule = $("input[type='radio'][name='simpleRule']:checked").val() === "on" ? true : false;
});

$(document).ready(function() {
  dataNew = JSON.parse(JSON.stringify(prefs.data));
  renderCatsSet();
  renderPerf();
  renderTagsSet();
});

$("#btn-reset-pop").click((event) => {
  $("#settings-pop").css("display", "none");
});

$("#btn-save-set").click((event) => {
  let filteredCats = dataNew.cats.filter(function (cat) {
    return cat !== null;
  });
  dataNew.cats = filteredCats;
  prefs.data = JSON.parse(JSON.stringify(dataNew));
  prefs.save();
  // TODO move one step up for compatibility with pages
  document.location.href = "index.html";
});

$("#btn-add-tag").click((event) => {
  var formName = "add-tag";
  $(".cat-alert").html("");

  // check all fields filled
  // var tagId = $("#edit-tag-select").prop("selectedIndex");
  if (document.forms["add-tag"]["tag_name"].value === "" ||
      document.forms["add-tag"]["tag_rule"].value === "" ||
      document.forms["add-tag"]["tag_color"].value === "" ||
      document.forms["add-tag"]["tag_order"].value === "") {
    $(".cat-alert").html("Fill all the fields in the form!");
    return false;
  }

  // check if already exists
  var exists = false;
  dataNew.tags.forEach(function(tag, id) {
    if (newTag) {
      if (document.forms["add-tag"]["tag_name"].value === tag["name"]) {
        exists = true;
      }
    } else {
      if (document.forms["add-tag"]["tag_name"].value === tag["name"] && parseInt(editTagId, 10) !== parseInt(id, 10)) {
        exists = true;
      }
    }
  })

  if (exists) {
    $(".cat-alert").html("Already exists");
    return false;
  }

  // check rule
  if (!/^(ti|au|abs){.*?}((\||\&)(\(|)((ti|au|abs){.*?})(\)|))*$/i.test(document.forms["add-tag"]["tag_rule"].value)) {
    $(".cat-alert").html("Check the rule syntax!");
    return false;
  }

  // check color
  if (!/^#[0-9A-F]{6}$/i.test(document.forms["add-tag"]["tag_color"].value)) {
    $(".cat-alert").html("Color should be in hex format: e.g. #aaaaaa");
    return false;
  }

  // check order
  if (!/[0-9]/i.test(document.forms["add-tag"]["tag_order"].value)) {
    $(".cat-alert").html("Order should be an integer");
    return false;
  }

  // document.forms["add-tag"]["tag_rule"].value = document.forms["add-tag"]["tag_rule"].value.replace(/([^\\])\\([^\\])/i, "$1\\\\$2");

  let TagDict = {"name": document.forms["add-tag"]["tag_name"].value,
                 "rule": document.forms["add-tag"]["tag_rule"].value,
                 "color": document.forms["add-tag"]["tag_color"].value
               };

  $(".cat-alert").html("");
  let order = parseInt(document.forms["add-tag"]["tag_order"].value, 10);
  if (!newTag) {
    dataNew.tags.splice(editTagId, 1);
  }
  dataNew.tags.splice(order, 0, TagDict);

  $("#btn-reset").click();

  renderTagsSet();
  return false;
})

$("#btn-reset").click((event) => {
  document.forms["add-tag"]["tag_name"].value = "";
  document.forms["add-tag"]["tag_rule"].value = "";
  document.forms["add-tag"]["tag_color"].value = "";
  document.forms["add-tag"]["tag_order"].value = "";
  $("#tag-color").css("background-color", "#fff");
  $("#tag-fields").prop("disabled", true);
});

$("#btn-rm-tag").click((event) => {
  $("#del-name").html("tag <strong>" + dataNew.tags[parseInt(editTagId, 10)].name + "</strong>");
  $("#btn-confirm").on("click", function() {
    $("#btn-reset").click();
    dataNew.tags.splice(editTagId, 1);
    $("#settings-pop").css("display", "none");
    renderTagsSet();
  });
  $("#settings-pop").css("display", "block");
});

$("#btn-close").click((event) => {
  $("#info-pop").css("display", "none");
});

$("#export-btn").click((event) => {
  $("#set-form").val(JSON.stringify(prefs.data));
  var copyText = $("#set-form");

  copyText.select();
  // copyText.setSelectionRange(0, 99999); /*For mobile devices*/

  document.execCommand("copy");
  $("#info-name").html("Settings are copied into clipboard.<br/> Now you can store them in the notebook, share with a  collegue, or import in the other browser");
  $("#info-pop").css("display", "block");
});

$("#import-btn").click((event) => {
  dataNew = JSON.parse($("#set-form").val());
  renderCatsSet();
  renderTagsSet();
  renderPerf();
  $("#info-name").html("Settings are imported. Please have a look at the field on this page and save is sutisfied.");
  $("#info-pop").css("display", "block");
});

$("#tag-color").change((event) => {
  $("#tag-color").css("background-color", $("#tag-color").val());
});

// ask fo settings save on leave
window.onbeforeunload = function(){
  if (JSON.stringify(dataNew) === JSON.stringify(prefs.data)) {
    return;
  }
  return "Settings unsaved";
};

