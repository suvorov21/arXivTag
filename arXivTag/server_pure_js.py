from os import path

from flask import Flask, send_from_directory, redirect, url_for
from flask_cors import CORS

app = Flask(__name__, instance_relative_config=True)
CORS(app)

@app.route('/')
def root():
    """Return html page."""
    return send_from_directory(path.join(app.root_path, 'static'),
                               'index.html')

@app.route('/arXivTag/')
def redir():
    """Compatibility with pages."""
    return redirect(url_for('root'))

@app.route('/index.html')
def redir_index():
    """Compatibility with pages."""
    return redirect(url_for('root'))

@app.route('/settings.html')
def settings():
    """Return settings page."""
    return send_from_directory(path.join(app.root_path, 'static'),
                               'settings.html')

@app.route('/help.html')
def help_page():
    """Return help page."""
    return send_from_directory(path.join(app.root_path, 'static'),
                               'help.html')

def run():
    """Start server."""
    app.config['DEBUG'] = True

    try:
        app.run(port=8880, host='127.0.0.1')
    except OSError:
        print(f'The adress {app.config["HOST"]}:{app.config["PORT"]} is likely in use.')
        print('1. Check that this particular app is not in progress.')
        print('2. If not, your system may use this adress for other purpose.')
        print('  Change the port and/or ip adress')


if __name__ == '__main__':
    run()
